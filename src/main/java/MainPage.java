import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import static org.testng.Assert.assertTrue;

public class MainPage extends PageObject {

    private final Actions action;

    @FindBy(xpath = "//h4[. = 'iPhone']/a")
    private WebElement productLink;

    @FindBy(xpath = "//div[@class='caption' and .//h4[.='iPhone']]//following-sibling::div//span[.='Add to Cart']")
    private WebElement productToCart;

    @FindBy(xpath = "//ul[@class='breadcrumb']/following-sibling::div[1]/a")
    private WebElement successMsg;

    public MainPage(WebDriver driver){
        super(driver);
        action = new Actions(driver);
        assertTrue(productToCart.isDisplayed());
    }

    public ProductPage moveToProductPage(){
        action.moveToElement(this.productLink).click().perform();
        return new ProductPage(driver);
    }

    public void addProductToCart(){
        action.moveToElement(this.productToCart).click().perform();
    }
}