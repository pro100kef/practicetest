import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class ConditionsClass {

    protected static WebDriver driver;

    @BeforeClass
    public static void preCond(){
        // Driver initialization.
        WebDriverManager.operadriver().setup();
        driver = new OperaDriver();
        driver.manage().window().maximize();
        driver.get("https://demo.opencart.com");

        // Preconditions -> Add product to the cart and move to it's page.
        MainPage mainPage = new MainPage(driver);
        mainPage.addProductToCart();
        ProductPage iPhonePage = mainPage.moveToProductPage();
    }

    @AfterClass
    public static void postCond(){
        // Postconditions -> Open Home page.
        driver.get("https://demo.opencart.com/index.php?route=common/home");
        driver.close();
        driver.quit();
    }
}
