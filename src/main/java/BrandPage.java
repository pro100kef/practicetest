import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.testng.Assert.assertTrue;

public class BrandPage extends PageObject {

    private final Actions action;

    @FindBy(xpath = "//div/h2")
    private WebElement brandName;

    @FindBy(xpath = "//div[@class='caption' and .//h4[.='iPhone']]//following-sibling::div//span[.='Add to Cart']")
    private WebElement productToCart;

    @FindBy(xpath = "//ul[@class='breadcrumb']/following-sibling::div[1]/a")
    private WebElement successMsg;

    @FindBy(xpath = "//div[@id='cart']/button")
    private WebElement cartBtn;

    @FindBy(xpath = "(//td[@class='text-right'])[1]")
    private WebElement cartProdQnt;

    @FindBy(xpath = "(//p[@class='text-right']//strong)[1]")
    private WebElement viewCartLink;

    public BrandPage(WebDriver driver){
        super(driver);
        action = new Actions(driver);
        assertTrue(brandName.isDisplayed());
    }

    protected void WaitForAJAX(){
        Wait wait = new WebDriverWait(driver, 3);
        wait.until(driver -> {
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            String active = jse.executeScript("return window.jQuery.active").toString();
            return active.equals("0");
        });
    }

    public String getBrandName(){
        WaitForAJAX();
        return brandName.getText();
    }

    public void addProductToCart(){
        action.moveToElement(this.productToCart).click().perform();
    }

    public String getProductFromMessage(){
        WaitForAJAX();
        return successMsg.getText();
    }

    public void cartBtnClick(){
        cartBtn.click();
    }

    public String getCartProdQntText(){
        return cartProdQnt.getText();
    }

    public void viewCartLinkClick(){
        viewCartLink.click();
    }
}