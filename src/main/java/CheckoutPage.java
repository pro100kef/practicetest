import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.testng.Assert.assertTrue;

public class CheckoutPage extends PageObject {

    @FindBy(xpath = "(//input[@class='form-control'])[1]")
    private WebElement prodQnt;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement updateBtn;

    public CheckoutPage(WebDriver driver) {
        super(driver);
        assertTrue(prodQnt.isDisplayed());
    }

    public void setProdQnt(Integer num){
        prodQnt.clear();
        prodQnt.sendKeys(num.toString());
    }

    public void updateBtnClick(){
        updateBtn.click();
    }

    public String getProdQnt(){
        return prodQnt.getAttribute("value");
    }
}
