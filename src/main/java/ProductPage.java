import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.testng.Assert.assertTrue;

public class ProductPage extends PageObject {

    @FindBy(xpath = "//div/h1[. = 'iPhone']/following-sibling::ul/li/a")
    private WebElement brandLink;

    public ProductPage(WebDriver driver) {
        super(driver);
        assertTrue(brandLink.isDisplayed());
    }

    public void clickOnBrand(){
        this.brandLink.click();
    }
}