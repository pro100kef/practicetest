import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CartTest extends ConditionsClass {

    @Test(priority = 1)
    public void openBrandProdPage(){
        ProductPage productPage = new ProductPage(driver);
        productPage.clickOnBrand();

        // Moving to the brand(Apple) products page.
        BrandPage brandPage = new BrandPage(driver);
        assertEquals("Apple", brandPage.getBrandName());
    }

    @Test(priority = 2)
    public void sameProdToCart(){
        BrandPage brandPage = new BrandPage(driver);
        brandPage.addProductToCart();
        assertEquals("iPhone", brandPage.getProductFromMessage());
    }

    @Test(priority = 3)
    public void prodQntInCart(){
        BrandPage brandPage = new BrandPage(driver);
        brandPage.cartBtnClick();
        assertEquals("x 2",brandPage.getCartProdQntText());
    }

    @Test(priority = 4)
    public void viewCartQntCheck(){
        BrandPage brandPage = new BrandPage(driver);
        brandPage.viewCartLinkClick();

        // Moving to the cart checkout page.
        CheckoutPage checkoutPage = new CheckoutPage(driver);
        assertEquals("2", checkoutPage.getProdQnt());
    }

    @Test(priority = 5)
    public void updateProdQnt(){
        CheckoutPage checkoutPage = new CheckoutPage(driver);
        checkoutPage.setProdQnt(1);
        checkoutPage.updateBtnClick();
    }
}